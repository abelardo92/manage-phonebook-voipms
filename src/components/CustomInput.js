import React from 'react';

const CustomInput = ({title, name, value, onChange}) => {
    return (
        <div className="Phonebook-field-container">
            <label>{title}:</label><br/>
            <input type="text" name={name} value={value} onChange={onChange} />
        </div>
    )
}
export default CustomInput
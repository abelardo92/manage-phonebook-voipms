import React from 'react'
import '../styles/Error.scss'

const Error = ({message}) => {
    return (
    <div className="PhonebookList-container error">
        <h1>Error: {message}</h1>
    </div>
    )
}

export default Error
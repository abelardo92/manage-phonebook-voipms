import React, {useEffect} from 'react';
import { Link } from 'react-router-dom';
import { useFetch } from 'use-http';
import voipms_url from "../config.js";

const PhonebookRow = ({phonebook, loadPhonebooks}) => {

    const {get, response, loading, error} = useFetch(voipms_url);

    const handleDelete = async id => {
        let phonebook_url = `/rest.php?api_username=abelardo-0@hotmail.com&api_password=ABcd1234&method=delPhonebook&phonebook=${phonebook.phonebook}`
        const result = await get(phonebook_url)
        if (response.ok) {
            if(result.status === 'success') {
                console.log("load phonebooks")
                console.log(result)
                // useEffect(() => { loadPhonebooks() }, [])
                await loadPhonebooks()
            }
        }
        // await loadPhonebooks()
    }

    return (
        <tr>
            <td>{phonebook.phonebook}</td>
            <td>{phonebook.name}</td>
            <td>{phonebook.number}</td>
            <td>{phonebook.callerid}</td>
            <td>{phonebook.note}</td>
            <td>
                <Link className="App-link" to={`/phonebook/${phonebook.phonebook}`}>Edit</Link>
                <button className="App-link App-link-delete" onClick={() => handleDelete(phonebook.phonebook)}>Delete</button>
            </td>
        </tr>
    )
}
export default PhonebookRow;
import React from 'react';
import PhonebookRow from './PhonebookRow';

const PhonebookTable = ({phonebooks, loadPhonebooks}) => {

    return (
        <table className="PhonebookList-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>CallerID</th>
                    <th>Note</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {phonebooks.map((phonebook) => (
                    <PhonebookRow key={phonebook.phonebook} phonebook={phonebook} loadPhonebooks={loadPhonebooks}/>
                ))}
            </tbody>
        </table>
    )
}

export default PhonebookTable;
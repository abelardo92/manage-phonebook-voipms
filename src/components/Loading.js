import React from 'react'

const Loading = () => {
    return (
    <div className="PhonebookList-container">
        <header className="d-flex">
            <h1>Loading...</h1>
        </header>
    </div>
    )
}

export default Loading
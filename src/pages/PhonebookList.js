import React, {useState, useEffect} from 'react';
import voipms_url from "../config";
import useFetch from 'use-http';
import { Link } from 'react-router-dom';
import PhonebookTable from '../components/PhonebookTable';
import Loading from '../components/Loading';
import Error from '../components/Error';
import "../styles/PhonebookList.scss";

const PhonebookList = () => {
    const [data, setData] = useState();

    const {get, response, loading, error} = useFetch(voipms_url);
    useEffect(() => { loadPhonebooks() }, [])

    async function loadPhonebooks() {
        const initialPhonebook = await get("/rest.php?api_username=abelardo-0@hotmail.com&api_password=ABcd1234&method=getPhonebook")
        console.log(initialPhonebook)
        if (response.ok) setData(initialPhonebook)
    }

    if(loading) return <Loading/>;

    if(error || data?.status !== 'success') return <Error message={data?.status}/>

    return (
        <div className="PhonebookList-container">
            <header className="d-flex">
                <h1>Phonebook Management</h1>
                <Link className="App-link" to="/phonebook/add">Add New</Link>
            </header>
            <PhonebookTable phonebooks={data.phonebooks} loadPhonebooks={loadPhonebooks} />
        </div>
    )
}
export default PhonebookList;
import React, {useState, useEffect} from 'react';
import { useParams, Link } from "react-router-dom";
import voipms_url from "../config";
import { useFetch } from 'use-http';
import Loading from '../components/Loading'
import CustomInput from '../components/CustomInput'
import '../styles/Phonebook.scss'

const Phonebook = () => {
    let { id } = useParams()
    const { get, response, loading, error } = useFetch(voipms_url)
    const [phonebook, setPhonebook] = useState({ id: 0, name: '', number: '', callerid: ''})
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const wrongPhonebookRequest = () => phonebook.status && phonebook.status !== 'success'

    useEffect(() => { loadPhonebook() }, []) // componentDidMount

    async function loadPhonebook() {
        if(id) {
            let phonebook_url = `/rest.php?api_username=abelardo-0@hotmail.com&api_password=ABcd1234&method=getPhonebook&phonebook=${id}`;
            const result = await get(phonebook_url)
            if (response.ok) {
                setPhonebook(result.status !== 'success' ? {status: result.status, message: result.message} : result.phonebooks[0])
            }
        } 
    }
    if(id) {
        if(loading) return <Loading/>;
        if(error || wrongPhonebookRequest()) return <p>Error! {phonebook?.message}</p>
    }

    const handleInputChange = event => {
        setPhonebook({
            ...phonebook,
            [event.target.name] : event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        let phonebook_url = `/rest.php?api_username=abelardo-0@hotmail.com&api_password=ABcd1234&method=setPhonebook&name=${phonebook.name}&number=${phonebook.number}&callerid=${phonebook.callerid}&note=${phonebook.note}`
        if(id) {
            phonebook_url += `&phonebook=${id}`
        }
        const result = await get(phonebook_url)
        if (response.ok) {
            console.log(phonebook_url)
            if(result.status === 'success') {
                setSuccessMessage("Registro actualizado")
                setErrorMessage('')
            } else {
                setSuccessMessage('')
                setErrorMessage(result.message)
            }
            // setPhonebook(result.status !== 'success' ? {status: result.status, message: result.message} : result.phonebooks[0])
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="PhonebookList-container">
                <div className="d-flex">
                    <h1>{id ? "Add Phonebook" : "Edit Phonebook"}</h1>
                    {errorMessage && <p>{errorMessage}</p>}
                    {successMessage && <p>{successMessage}</p>}
                </div>
                <div className="d-flex Phonebook-Row">
                    <CustomInput title="Name" name="name" value={phonebook.name} onChange={handleInputChange}/>
                    <CustomInput title="Number" name="number" value={phonebook.number} onChange={handleInputChange}/>
                    <CustomInput title="Caller ID" name="callerid" value={phonebook.callerid} onChange={handleInputChange}/>
                </div>
                <div className="d-flex Phonebook-Row">
                    <CustomInput title="Note" name="note" value={phonebook.note} onChange={handleInputChange}/>
                </div>
                <div className="d-flex Phonebook-Row">
                    <Link className="App-link" to="/">Go Back</Link>
                    <button type="submit" className="App-link">Save</button>
                </div>
            </div>
        </form>
    )
}
export default Phonebook;
import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./styles/App.scss";
import PhonebookList from './pages/PhonebookList';
import Phonebook from './pages/Phonebook';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={PhonebookList}/>
        <Route exact path="/phonebook/add" component={Phonebook}/>
        <Route exact path="/phonebook/:id" component={Phonebook}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
